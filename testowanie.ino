#include <WiFiManager.h>  //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <Firebase_ESP_Client.h>
#include <AccelStepper.h>
#include <ArduinoJson.h>
#include <Preferences.h>

// FireBeetle-ESP32

// FIREBASE
// Provide the token generation process info.
#include <addons/TokenHelper.h>
// Provide the RTDB payload printing info and other helper functions.
#include <addons/RTDBHelper.h>
#define API_KEY "TODO"
#define DATABASE_URL "https://TODO-default-rtdb.europe-west1.firebasedatabase.app/"  //<databaseName>.firebaseio.com or <databaseName>.<region>.firebasedatabase.app
#define USER_EMAIL "TODO"
#define USER_PASSWORD "TODO"
FirebaseData stream;
FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;
String uid;
volatile bool changed = false;
//StaticJsonDocument<200> changedDoc;
FirebaseJson json;


// preferences
Preferences preferences;

// stepper
int stepperSpeed = 0;
int stepperSpeedMax = 0;
int stepperPower = HIGH;

int enStepperPin = 23; // 10;                             // pin odpowiada za wlaczenie silniki
AccelStepper stepper(AccelStepper::DRIVER, 22 /*8*/, 21 /*9*/);  //StepPin 8 and DirPin 9
int MS1Pin = 17; // 5;
int MS2Pin = 18; // 6;
int MS3Pin = 19; // 7;
int ms1 = LOW;
int ms2 = LOW;
int ms3 = LOW;

void streamCallback(FirebaseStream data) {
  Serial.printf("streamPath, %s\ndataPath, %s\ndataType, %s\neventType, %s\n\n",
                data.streamPath().c_str(),  // /oeP3606ru4RMZCcV5RrmXGqrUgI2/settings
                data.dataPath().c_str(),    // /calPH
                data.dataType().c_str(),    // string
                data.eventType().c_str());  // put jak dodaje to /patch i zwraca {"pH":8.4}
  //printResult(data);                        // see addons/RTDBHelper.h
  if (data.eventType() == "put" && data.dataPath() == "/stepperSpeed") {
    preferences.putUInt("stepperSpeed", data.to<int>());
  } else if (data.eventType() == "put" && data.dataPath() == "/stepperSpeedMax") {
    preferences.putUInt("stepperSpeedMax", data.to<int>());
  } else if (data.eventType() == "put" && data.dataPath() == "/ms1") {
    preferences.putUInt("ms1", data.to<int>());
  } else if (data.eventType() == "put" && data.dataPath() == "/ms2") {
    preferences.putUInt("ms2", data.to<int>());
  } else if (data.eventType() == "put" && data.dataPath() == "/ms3") {
    preferences.putUInt("ms3", data.to<int>());
  } else if (data.eventType() == "put" && data.dataPath() == "/stepperPower") {
    preferences.putUInt("stepperPower", data.to<int>());
  } else if (data.eventType() == "patch" && data.dataPath() == "/" && data.dataType() == "json") {
    FirebaseJson *json = data.to<FirebaseJson *>();
    size_t len = json->iteratorBegin();
    FirebaseJson::IteratorValue value;
    //json->toString(Serial, true);
    for (size_t i = 0; i < len; i++) {
      value = json->valueAt(i);
      preferences.putUInt(value.key.c_str(), value.value.toInt());
    }
    json->iteratorEnd();
    json->clear();
  }  
  changed = true;
}

void streamTimeoutCallback(bool timeout) {
  if (timeout)
    Serial.println("stream timed out, resuming...\n");

  if (!stream.httpConnected())
    Serial.printf("error code: %d, reason: %s\n\n", stream.httpCode(), stream.errorReason().c_str());
}


void setup() {
  Serial.begin(9600);
  // WIFI
  WiFiManager wm;
  bool res;
  res = wm.autoConnect("ReefNet2");
  if (!res) {
    Serial.println("Failed to connect");
    // ESP.restart();
  } else {
    //if you get here you have connected to the WiFi
    Serial.println("connected...yeey :)");
  }

  // FIREBASE
  Serial.printf("Firebase Client v%s\n\n", FIREBASE_CLIENT_VERSION);
  config.api_key = API_KEY;
  auth.user.email = USER_EMAIL;
  auth.user.password = USER_PASSWORD;
  config.database_url = DATABASE_URL;
  config.token_status_callback = tokenStatusCallback;  // see addons/TokenHelper.h
                                                       //#if defined(ESP8266)
  // In ESP8266 required for BearSSL rx/tx buffer for large data handle, increase Rx size as needed.
  // 2048 Rx buffer size in bytes from 512 - 16384 , 2048 /* Tx buffer size in bytes from 512 - 16384
  //fbdo.setBSSLBufferSize(2048, 2048);
  //#endif
  // Recommend for ESP8266 stream, adjust the buffer size to match your stream data size
  //#if defined(ESP8266)
  // 2048 Rx in bytes, 512 - 16384 , 512 Tx in bytes, 512 - 16384
  //stream.setBSSLBufferSize(2048, 512 Tx in bytes, 512 - 16384);
  //#endif
  // Limit the size of response payload to be collected in FirebaseData
  fbdo.setResponseSize(2048);
  Firebase.begin(&config, &auth);
  Firebase.reconnectWiFi(true);

  // Getting the user UID might take a few seconds
  Serial.println("Getting User UID");
  while ((auth.token.uid) == "") {
    Serial.print('.');
    delay(1000);
  }
  // Print user UID
  uid = auth.token.uid.c_str();
  Serial.print("User UID: ");
  Serial.print(uid);

  if (!Firebase.RTDB.beginStream(&stream, "/" + uid + "/settingsKalkwasser150")) {
    Serial.printf("sream begin error, %s\n\n", stream.errorReason().c_str());
  }
  Firebase.RTDB.setStreamCallback(&stream, streamCallback, streamTimeoutCallback);
  Firebase.setDoubleDigits(5);
  config.timeout.serverResponse = 10 * 1000;  

  // INIT DATA
  /*
  json.set("ms1", LOW);
  preferences.putUInt("ms1", LOW);
  json.set("ms2", LOW);
  preferences.putUInt("ms2", LOW);
  json.set("ms3", LOW);
  preferences.putUInt("ms3", LOW);
  json.set("stepperSpeedMax", 3200);
  preferences.putUInt("stepperSpeedMax", 3200);
  json.set("stepperSpeed", 1000);
  preferences.putUInt("stepperSpeed", 1000);
  json.set("stepperPower", LOW);
  preferences.putUInt("stepperPower", LOW);
  json.set(F("ts/.sv"), F("timestamp"));
  Serial.printf("Set json... %s\n", Firebase.RTDB.set(&fbdo, "/" + uid + "/settingsKalkwasser150", &json) ? "ok" : fbdo.errorReason().c_str());
  json.clear();
  */
  

  // preferences
  preferences.begin("ReefNet2", false);
  // Remove all preferences under the opened namespace
  //preferences.clear();
  // Or remove the counter key only
  //preferences.remove("counter");  
  setValuesFromPreferences();

  // stepper
  pinMode(enStepperPin, OUTPUT);
  pinMode(MS1Pin, OUTPUT);
  pinMode(MS2Pin, OUTPUT);
  pinMode(MS3Pin, OUTPUT);
  setupStepper();
  Serial.println("START");
}

void loop() {
  stepper.setSpeed(stepperSpeed);
  stepper.runSpeed();

  if (changed) {
    changed = false;
    Serial.println("CHANGED");
    setValuesFromPreferences();
    setupStepper();
    printPreferences();
  }
}

void setupStepper() {
  digitalWrite(MS1Pin, ms1);
  digitalWrite(MS2Pin, ms2);
  digitalWrite(MS3Pin, ms3);
  stepper.setMaxSpeed(stepperSpeedMax);
  digitalWrite(enStepperPin, stepperPower);  // LOW - wlacz
}

void setValuesFromPreferences() {
  stepperSpeed = preferences.getUInt("stepperSpeed");
  stepperSpeedMax = preferences.getUInt("stepperSpeedMax");
  stepperPower = preferences.getUInt("stepperPower");
  ms1 = preferences.getUInt("ms1");
  ms2 = preferences.getUInt("ms2");
  ms3 = preferences.getUInt("ms3");
}

void printPreferences() {
  Serial.println("");
  Serial.print("preferences stepperSpeed: ");
  Serial.println(stepperSpeed);
  Serial.print("preferences stepperSpeedMax: ");
  Serial.println(stepperSpeedMax);
  Serial.print("preferences stepperPower: ");
  Serial.println(stepperPower);
  Serial.println(digitalRead(enStepperPin));
  Serial.print("preferences ms1: ");
  Serial.println(ms1);
  Serial.println(digitalRead(MS1Pin));
  Serial.print("preferences ms2: ");
  Serial.println(ms2);
  Serial.println(digitalRead(MS2Pin));
  Serial.print("preferences ms3: ");
  Serial.println(ms3);
  Serial.println(digitalRead(MS3Pin));
  Serial.println("");
}
